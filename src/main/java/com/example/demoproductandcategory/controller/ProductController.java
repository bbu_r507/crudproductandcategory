package com.example.demoproductandcategory.controller;

import com.example.demoproductandcategory.constan.Constants;
import com.example.demoproductandcategory.models.Product;
import com.example.demoproductandcategory.repository.CategoryRepository;
import com.example.demoproductandcategory.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/products")
@RequiredArgsConstructor
public class ProductController {
    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;
    @GetMapping("/product")
    public String index(Model model){
        model.addAttribute("products",productRepository.findAllByStatusInOrderByIdDesc(Constants.getAllStatusString()));
        return ("product/index");
    }
    @GetMapping("/create")
    public String create(Model model){
        Product product = new Product();
        product.setCategories(categoryRepository.findAllByStatus("ACT"));
        product.setStatusList(Constants.getAllStatus());
        product.setStatusListType(Constants.getAllStatusListType());
        model.addAttribute("product" , product);
        return ("product/create");

    }
    @PostMapping("/create")
    public String create(Model model, @ModelAttribute("product") Product product){
        productRepository.save(product);
        return ("redirect:/products/product");

    }
    @GetMapping("/edit/{id}")
    public String edit(Model model, @PathVariable("id") Integer id){
        Product product = productRepository.findById(id).orElse(null);
        if (product != null){
            product.setCategories(categoryRepository.findAllByStatus("ACT"));
            product.setStatusList(Constants.getAllStatus());
            product.setStatusListType(Constants.getAllStatusListType());
            model.addAttribute("product" , product);
            return ("product/create");
        }
        return ("redirect:/products/product");

    }
    @GetMapping("/delete/{id}")
    public String delete(Model model, @PathVariable("id") Integer id){
        Product product = productRepository.findById(id).orElse(null);
        if (product != null){
           product.setStatus("DEL");
           productRepository.save(product);
        }
        return ("redirect:/products/product");
    }
    @GetMapping("/disable/{id}")
    public String disable(Model model, @PathVariable("id") Integer id){
        Product product = productRepository.findById(id).orElse(null);
        if (product != null){
            product.setStatus("DSL");
            productRepository.save(product);
        }
        return ("redirect:/products/product");
    }
    @GetMapping("/active/{id}")
    public String active(Model model, @PathVariable("id") Integer id){
        Product product = productRepository.findById(id).orElse(null);
        if (product != null){
            product.setStatus("ACT");
            productRepository.save(product);
        }
        return ("redirect:/products/product");
    }
}
